﻿//Implement Brain Game functionality
//JS  Brain Game version 1.0
/*******************************************************
Created By: Yashwant Kumar
Created Date:25-07-2016
Description:Implement Brain Game functionality
********************************************************/
/*******************************************************
Modified By: 
Modified Date:
Description:Implement Brain Game functionality
********************************************************/
//*****************Global Variables Declaration*********
var correct_Level1 = 0;
var correct_Level2 = 0;
var correct_Level3 = 0;
var result = 0;

var tempL1 = 30; toL1 = null; intL1 = null; shutdownEngagedL1 = null;
var tempL2 = 30; toL2 = null; intL2 = null; shutdownEngagedL2 = null;
var tempL3 = 30; toL3 = null; intL3 = null; shutdownEngagedL3 = null;
var tempScreen5 = 30; toScreen5 = null; intScreen5 = null; shutdownEngagedScreen5 = null;
var tempScreen6 = 5; toScreen6 = null; intScreen6 = null;
var tempScreen7 = 30; toScreen7 = null; intScreen7 = null; shutdownEngagedScreen7 = null;
var tempScreen4_7 = 30; toScreen4_7 = null; intScreen4_7 = null; shutdownEngagedScreen4_7 = null;
var tempScreen4_6 = 5; toScreen4_6 = null; intScreen4_6 = null;
var $check;
var $box;
var group;
var CheckedValue;
var JsonDataResult;
var resultToBind = "";
var ques = 0;
var ans = 0;
var ans1 = 0; ans2 = 0; ans3 = 0; ans4 = 0;
var LogicalCorrectAns = 0; LogicalCheckedQ1 = 0; LogicalCheckedQ2 = 0; LogicalCheckedQ3 = 0; LogicalCheckedQ4 = 0;
var Age = 0;
//*************End of Global Variable declaration********

$(document).ready(function () {
    BrainGame.Init();
});
var BrainGame = {
    Init: function () {
        BrainGame.Bind.Init();
    },
    Bind: {
        Init: function () {
            BrainGame.Bind.PreInit();
            BrainGame.Bind.Click.Init();
            //BrainGame.Bind.Ajax.FirstLogical();
        },
        PreInit: function () {
            $("#divScreenSize1").css("display", "block");
            $("#divScreenSize2").css("display", "none");
            $("#divScreenSize3").css("display", "none");
            $("#divScreenSize4-1").css("display", "none");
            $("#divScreenSize4-2").css("display", "none");
            $("#divScreenSize4-3").css("display", "none");
            $("#divScreenSize4-4").css("display", "none");
            $("#divScreenSize4-5").css("display", "none");
            $("#divScreenSize4-6").css("display", "none");
            $("#divScreenSize4-7").css("display", "none");
            $("#divScreenSize4-8").css("display", "none");

            $("#divscreen5").css("display", "none");
            $("#divscreen6").css("display", "none");
            $("#divscreen7").css("display", "none");
            $("#divLevel1").css("display", "none");
            $("#divLevel2").css("display", "none");
            $("#divLevel3").css("display", "none");
            $("#divLevelresult").css("display", "none");

            BrainGame.Bind.Reset();

        },
        Reset: function () {
            correct_Level1 = 0; correct_Level2 = 0; correct_Level3 = 0;
            result = 0;
            ques = 0; ans = 0;
            ans1 = 0; ans2 = 0; ans3 = 0; ans4 = 0;
            LogicalCorrectAns = 0;
            LogicalCheckedQ1 = 0; LogicalCheckedQ2 = 0; LogicalCheckedQ3 = 0; LogicalCheckedQ4 = 0;

            clearTimeout(toL1);
            clearInterval(intL1);
            clearTimeout(toL2);
            clearInterval(intL2);
            clearTimeout(toL3);
            clearInterval(intL3);
            clearTimeout(toScreen5);
            clearInterval(intScreen5);
            clearTimeout(toScreen6);
            clearInterval(intScreen6);
            clearTimeout(toScreen7);
            clearInterval(intScreen7);

            clearTimeout(toScreen4_6);
            clearInterval(intScreen4_6);
            clearTimeout(toScreen4_7);
            clearInterval(intScreen4_7);

            $("#txtfirst").val("");
            $("#txtsecond").val("");
            $("#txtAge").val("");
            //Timer class Reset

            $("#ClickMeLevel1").removeClass("timer-watch2");
            $("#ClickMeLevel2").removeClass("timer-watch2");
            $("#ClickMeLevel3").removeClass("timer-watch2");
            $("#ClickMeScreen5").removeClass("timer-watch2");
            $("#ClickMeScreen7").removeClass("timer-watch2");
            $("#ClickMe").removeClass("timer-watch2");

            //Checkbox Reset
            // $("#rcAgree").
            $('#rcAgree').attr('checked', false);
            $('input:checkbox[name=rc1]').attr('checked', false);
            $('input:checkbox[name=rc2]').attr('checked', false);
            $('input:checkbox[name=rc3]').attr('checked', false);
            $('input:checkbox[name=rc4]').attr('checked', false);

            //FadeOut Level boxes   
            //Level-1
            $('#first').removeClass('dactive-bg'); $('#first').addClass('active-bg');
            $('#second').removeClass('active-bg'); $('#second').addClass('dactive-bg'); $('#second').removeClass('box2');
            $('#third').removeClass('active-bg'); $('#third').addClass('dactive-bg'); $('#third').removeClass('box3');
            $('#four').removeClass('active-bg'); $('#four').addClass('dactive-bg'); $('#four').removeClass('box4');
            $('#five').removeClass('active-bg'); $('#five').addClass('dactive-bg'); $('#five').removeClass('box5');
            $('#six').removeClass('active-bg'); $('#six').addClass('dactive-bg'); $('#six').removeClass('box6');
            $('#seven').removeClass('active-bg'); $('#seven').addClass('dactive-bg'); $('#seven').removeClass('box7');
            $('#eight').removeClass('active-bg'); $('#eight').addClass('dactive-bg'); $('#eight').removeClass('box8');
            $('#nine').removeClass('active-bg'); $('#nine').addClass('dactive-bg'); $('#nine').removeClass('box9');
            $('#ten').removeClass('active-bg'); $('#ten').addClass('dactive-bg'); $('#ten').removeClass('box10');
            $('#eleven').removeClass('active-bg'); $('#eleven').addClass('dactive-bg'); $('#eleven').removeClass('box11');
            $('#twelve').removeClass('active-bg'); $('#twelve').addClass('dactive-bg'); $('#twelve').removeClass('box12');
            //Level-2

            $('#first_Level2').removeClass('dactive-bg'); $('#first_Level2').addClass('active-bg');
            $('#second_Level2').removeClass('active-bg'); $('#second_Level2').addClass('dactive-bg'); $('#second_Level2').removeClass('box2');
            $('#third_Level2').removeClass('active-bg'); $('#third_Level2').addClass('dactive-bg'); $('#third_Level2').removeClass('box3');
            $('#four_Level2').removeClass('active-bg'); $('#four_Level2').addClass('dactive-bg'); $('#four_Level2').removeClass('box4');
            $('#five_Level2').removeClass('active-bg'); $('#five_Level2').addClass('dactive-bg'); $('#five_Level2').removeClass('box5');
            $('#six_Level2').removeClass('active-bg'); $('#six_Level2').addClass('dactive-bg'); $('#six_Level2').removeClass('box6');
            $('#seven_Level2').removeClass('active-bg'); $('#seven_Level2').addClass('dactive-bg'); $('#seven_Level2').removeClass('box7');
            $('#eight_Level2').removeClass('active-bg'); $('#eight_Level2').addClass('dactive-bg'); $('#eight_Level2').removeClass('box8');
            $('#nine_Level2').removeClass('active-bg'); $('#nine_Level2').addClass('dactive-bg'); $('#nine_Level2').removeClass('box9');
            $('#ten_Level2').removeClass('active-bg'); $('#ten_Level2').addClass('dactive-bg'); $('#ten_Level2').removeClass('box10');
            $('#eleven_Level2').removeClass('active-bg'); $('#eleven_Level2').addClass('dactive-bg'); $('#eleven_Level2').removeClass('box11');
            $('#twelve_Level2').removeClass('active-bg'); $('#twelve_Level2').addClass('dactive-bg'); $('#twelve_Level2').removeClass('box12');
            $('#thirteen_Level2').removeClass('active-bg'); $('#thirteen_Level2').addClass('dactive-bg'); $('#thirteen_Level2').removeClass('box13');
            $('#fourteen_Level2').removeClass('active-bg'); $('#fourteen_Level2').addClass('dactive-bg'); $('#fourteen_Level2').removeClass('box14');
            $('#fifthteen_Level2').removeClass('active-bg'); $('#fifthteen_Level2').addClass('dactive-bg'); $('#fifthteen_Level2').removeClass('box15');
            $('#sixteen_Level2').removeClass('active-bg'); $('#sixteen_Level2').addClass('dactive-bg'); $('#sixteen_Level2').removeClass('box16');
            //Level-3
            $('#first_Level3').removeClass('dactive-bg'); $('#first_Level3').addClass('active-bg');
            $('#second_Level3').removeClass('active-bg'); $('#second_Level3').addClass('dactive-bg'); $('#second_Level3').removeClass('fade2');
            $('#third_Level3').removeClass('active-bg'); $('#third_Level3').addClass('dactive-bg'); $('#third_Level3').removeClass('fade3');
            $('#four_Level3').removeClass('active-bg'); $('#four_Level3').addClass('dactive-bg'); $('#four_Level3').removeClass('fade4');
            $('#five_Level3').removeClass('active-bg'); $('#five_Level3').addClass('dactive-bg'); $('#five_Level3').removeClass('fade5');
            $('#six_Level3').removeClass('active-bg'); $('#six_Level3').addClass('dactive-bg'); $('#six_Level3').removeClass('fade6');
            $('#seven_Level3').removeClass('active-bg'); $('#seven_Level3').addClass('dactive-bg'); $('#seven_Level3').removeClass('fade7');
            $('#eight_Level3').removeClass('active-bg'); $('#eight_Level3').addClass('dactive-bg'); $('#eight_Level3').removeClass('fade8');
            $('#nine_Level3').removeClass('active-bg'); $('#nine_Level3').addClass('dactive-bg'); $('#nine_Level3').removeClass('fade9');
            $('#ten_Level3').removeClass('active-bg'); $('#ten_Level3').addClass('dactive-bg'); $('#ten_Level3').removeClass('fade10');
            $('#eleven_Level3').removeClass('active-bg'); $('#eleven_Level3').addClass('dactive-bg'); $('#eleven_Level3').removeClass('fade11');
            $('#twelve_Level3').removeClass('active-bg'); $('#twelve_Level3').addClass('dactive-bg'); $('#twelve_Level3').removeClass('fade12');
            $('#thirteen_Level3').removeClass('active-bg'); $('#thirteen_Level3').addClass('dactive-bg'); $('#thirteen_Level3').removeClass('fade13');
            $('#fourteen_Level3').removeClass('active-bg'); $('#fourteen_Level3').addClass('dactive-bg'); $('#fourteen_Level3').removeClass('fade14');
            $('#fifthteen_Level3').removeClass('active-bg'); $('#fifthteen_Level3').addClass('dactive-bg'); $('#fifthteen_Level3').removeClass('fade15');
            $('#sixteen_Level3').removeClass('active-bg'); $('#sixteen_Level3').addClass('dactive-bg'); $('#sixteen_Level3').removeClass('fade16');

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = mm + '/' + dd + '/' + yyyy;
            $("#spanDate").html(today);
        },
        Exit: function () {
            BrainGame.Bind.PreInit();
        },
        Ajax: {
            FirstLogical: function (age) {
                //alert(age)
//                $.ajax({
//                    type: "POST",
//                    url: "http://54.164.93.219/kepish/api/questions/" + age,
//                    contentType: "application/json; charset=utf-8",
//                    dataType: "json",
//                    data: "{}",
//                    success: function (response) {
//                        //alert("sucess=" + JSON.stringify(response));
////                       JsonDataResult = JSON.stringify(JSON.stringify(response));
////                       JsonDataResult = JSON.parse(JsonDataResult);
//                    } //close sucess
//                });
                resultToBind = "";
                ques = 0;
                ans = 0;
            BrainGame.Bind.JsonData();//This line do comment when you enable Ajax code
                $.each(JsonDataResult, function (questionId, question) {
                    ques = ques + 1;
                    resultToBind = resultToBind + "<div class=screen-form>" +
                                "<div class='content-screen5'>" +
                                "<p>1." + question.question_description + "</p>" +
                                "<div class='checkbox-btn'>"
                    $.each(question.options, function (optionId, optionData) {
                        ans = ans + 1;
                        resultToBind = resultToBind + "<input type='checkbox' id=rc" + ans + " name=rc" + ques + ">" +
                                "<label for=rc" + ans + " ans=" + optionData.is_correct + " >" + optionData.option_value + "</label>" +
                                "<div class='clearfix'></div>"
                    });
                    resultToBind = resultToBind + "</div></div></div>";

                });
                $("#FirstLogicalAllQuestion").html(resultToBind);
                BrainGame.Bind.Event.PreInit();
            },
            SecondLogical: function (age) {
//                $.ajax({
//                    type: "POST",
//                    url: "http://54.164.93.219/kepish/api/questions/" + age,
//                    contentType: "application/json; charset=utf-8",
//                    dataType: "json",
//                    data: "{}",
//                    success: function (response) {
//                        //alert("sucess=" + JSON.stringify(response));
////                       JsonDataResult = JSON.stringify(response);
////                       JsonDataResult=JSON.parse(JsonDataResult);
//                    } //close sucess
//                });
                resultToBind = "";
                ques = 2;
                ans = 8;
                BrainGame.Bind.JsonData();//This line do comment when you enable Ajax code
                $.each(JsonDataResult, function (questionId, question) {
                    ques = ques + 1;
                    resultToBind = resultToBind + "<div class=screen-form>" +
                                "<div class='content-screen5'>" +
                                "<p>1." + question.question_description + "</p>" +
                                "<div class='checkbox-btn'>"
                    $.each(question.options, function (optionId, optionData) {
                        ans = ans + 1;
                        resultToBind = resultToBind + "<input type='checkbox' id=rc" + ans + " name=rc" + ques + ">" +
                                "<label for=rc" + ans + " ans=" + optionData.is_correct + " >" + optionData.option_value + "</label>" +
                                "<div class='clearfix'></div>"
                    });
                    resultToBind = resultToBind + "</div></div></div>";

                });
                $("#SecondLogicalAllQuestion").html(resultToBind);
                BrainGame.Bind.Event.PreInit();
            }
        },
        HTML: {
            FirstQuestion: function () {

            },
            SecondQuestion: function () {
            }
        },
        JsonData: function () {
            JsonDataResult = [{
                "id": 20,
                "question_description": " What is 4x6?",
                "options": [{
                    "option_value": " 21",
                    "is_correct": false,
                    "question_id": 20
                }, {
                    "option_value": " 24",
                    "is_correct": true,
                    "question_id": 20
                }, {
                    "option_value": " 30",
                    "is_correct": false,
                    "question_id": 20
                }, {
                    "option_value": " 10",
                    "is_correct": false,
                    "question_id": 20
                }]
            }, {
                "id": 17,
                "question_description": " What is 3.1 + 1.9?",
                "options": [{
                    "option_value": " 5.0",
                    "is_correct": true,
                    "question_id": 17
                }, {
                    "option_value": " 4.0",
                    "is_correct": false,
                    "question_id": 17
                }, {
                    "option_value": " 1.2",
                    "is_correct": false,
                    "question_id": 17
                }, {
                    "option_value": " 4.9",
                    "is_correct": false,
                    "question_id": 17
                }]
            }]
        },
        Event: {
            PreInit: function () {
                $("input:checkbox").on('click', function () {
                    $box = $(this);
                    if ($box.is(":checked")) {
                        group = "input:checkbox[name='" + $box.attr("name") + "']";

                        $(group).prop("checked", false);
                        $box.prop("checked", true);
                    } else {
                        $box.prop("checked", false);
                    }
                });
                BrainGame.Bind.Event.Init();
            },
            Init: function () {
                BrainGame.Bind.Event.FirstLogical.Init();
                BrainGame.Bind.Event.SecondLogical.Init();
            },
            FirstLogical: {
                Init: function () {
                    BrainGame.Bind.Event.FirstLogical.FirstCheckBox();
                    BrainGame.Bind.Event.FirstLogical.SecondCheckBox();
                },
                FirstCheckBox: function () {
                    $("input[name=rc1]").click(function () {
                        $box = $(this);
                        if ($box.is(":checked")) {
                            CheckedValue = "";
                            CheckedValue = $("label[for='" + this.id + "']").attr("ans");
                            if (CheckedValue == "true") {
                                ans1 = 1;
                                LogicalCheckedQ1 = 1;
                            }
                            else {
                                ans1 = 0;
                                LogicalCheckedQ1 = 1;
                            }
                        }
                        else {
                            ans1 = 0;
                            LogicalCheckedQ1 = 0;
                        }
                    });
                },
                SecondCheckBox: function () {
                    $("input[name=rc2]").click(function () {
                        $box = $(this);
                        if ($box.is(":checked")) {
                            CheckedValue = "";
                            CheckedValue = $("label[for='" + this.id + "']").attr("ans");
                            if (CheckedValue == "true") {
                                ans2 = 1;
                                LogicalCheckedQ2 = 1;
                            }
                            else {
                                ans2 = 0;
                                LogicalCheckedQ2 = 1;
                            }
                        }
                        else {
                            LogicalCheckedQ2 = 0;
                            ans2 = 0;
                        }
                    });
                }
            },//Close FirstLogical
            SecondLogical: {
                Init: function () {
                    BrainGame.Bind.Event.SecondLogical.FirstCheckBox();
                    BrainGame.Bind.Event.SecondLogical.SecondCheckBox();
                },
                FirstCheckBox: function () {
                    $("input[name=rc3]").click(function () {
                        $box = $(this);
                        if ($box.is(":checked")) {
                            CheckedValue = "";
                            CheckedValue = $("label[for='" + this.id + "']").attr("ans");
                            if (CheckedValue == "true") {
                                ans3 = 1;
                                LogicalCheckedQ3 = 1;
                            }
                            else {
                                ans3 = 0;
                                LogicalCheckedQ3 = 1;
                            }
                        }
                        else {
                            LogicalCheckedQ3 = 0;
                            ans3 = 0;
                        }
                    });
                },
                SecondCheckBox: function () {
                    $("input[name=rc4]").click(function () {
                        $box = $(this);
                        if ($box.is(":checked")) {
                            CheckedValue = "";
                            CheckedValue = $("label[for='" + this.id + "']").attr("ans");
                            if (CheckedValue == "true") {
                                ans4 = 1;
                                LogicalCheckedQ4 = 1;
                            }
                            else {
                                ans4 = 0;
                                LogicalCheckedQ4 = 1;
                            }
                        }
                        else {
                            LogicalCheckedQ4 = 0;
                            ans4 = 0;
                        }
                    });
                }
            }//Close SecondLogical
        },//Close Event
        Click: {
            Init: function () {
                BrainGame.Bind.Click.LoginNext();
                BrainGame.Bind.Click.More();
            },
            LoginNext: function () {
                $("#btnNext").click(function () {
                    if (BrainGame.Bind.Vaildation.RequiredField() == true) {
                        $("#ErrorfirstLastName").text("");
                        if (BrainGame.Bind.Vaildation.AgeLimit() == true) {
                            $("#ErrorfirstLastName").text("");
                            if (BrainGame.Bind.Vaildation.BaseLinePostInjured() == true) {
                                $("#Errorchk").text("");
                                Age = $("#txtAge").val();
                                BrainGame.Bind.Reset();
                                BrainGame.Bind.ShowHide.Screen1();
                            }
                            else {
                                $("#Errorchk").text("Please select test type.");
                            }
                        }
                        else {
                            $("#ErrorfirstLastName").text("Age Limit should be 5-17 years.");
                        }
                    }
                    else {
                        $("#ErrorfirstLastName").text("Please enter your first, last initial and age.");
                    }
                });
            },
            More: function () {
                $('#see_more').click(function () {
                    $('#see_more').hide();
                    $('#be_hide').show();
                });
            }
        },
        Vaildation: {
            Agree: function () {
                if ($('#rcAgree').is(':checked')) {
                    BrainGame.Bind.ShowHide.Screen2();
                } else {
                    alert('Please check agree.');
                }
            },

            BaseLinePostInjured: function () {
                if ($('#rcBaseLine').is(':checked') || $('#rcPostInjury').is(':checked')) {
                    return true;
                }
                else {
                    return false;
                }
            },
            RequiredField: function () {
                if ($("#txtfirst").val() == "" || $("#txtsecond").val() == "" || $("#txtAge").val() == "") {
                    return false;
                }
                else {
                    return true;
                }
            },
            AgeLimit: function () {
                var agelimit = $("#txtAge").val();
                if (agelimit >= 5 && agelimit <= 17) {
                    return true;
                }
                else {
                    return false;
                }
            }

        },
        Timer: {
            Screen4_6: function () {
                tempScreen4_6 = 5; toScreen4_6 = null; intScreen4_6 = null;
                toScreen4_6 = setTimeout(function () {
                    intScreen4_6 = setInterval(function () {
                        tempScreen4_6--;
                        if (tempScreen4_6 == 0) {
                            BrainGame.Bind.ShowHide.Screen4_6();
                        }
                    }, 900);
                }, 500);

            },
            Screen4_7: function () {
                clearTimeout(toScreen4_6);
                clearInterval(intScreen4_6);
                tempScreen4_7 = 30; toScreen4_7 = null; intScreen4_7 = null;
                //temp--;
                $("#Temp").html(tempScreen4_7);
                toScreen4_7 = setTimeout(function () {
                    intScreen4_7 = setInterval(function () {
                        tempScreen4_7--;
                        if (tempScreen4_7 <= 5) {
                            var shutdownEngagedScreen4_7 = $("#shutdown_engaged")[0];
                            shutdownEngagedScreen4_7.play();
                            $("#ClickMe").addClass("timer-watch2");
                        }
                        if (tempScreen4_7 == 0) {
                            BrainGame.Bind.ShowHide.Screen4_7();
                        }
                        $("#Temp").html(tempScreen4_7);
                    }, 900);
                }, 500);
            },
            Screen5: function () {
                clearTimeout(toScreen4_7);
                clearInterval(intScreen4_7);
                tempScreen5 = 30; toScreen5 = null; intScreen5 = null;
                //temp--;
                $("#TempScreen5").html(tempScreen5);
                toScreen5 = setTimeout(function () {
                    intScreen5 = setInterval(function () {
                        tempScreen5--;
                        if (tempScreen5 <= 5) {
                            shutdownEngagedScreen5 = $("#shutdown_engaged")[0];
                            shutdownEngagedScreen5.play();
                            $("#ClickMeScreen5").addClass("timer-watch2");
                        }
                        if (tempScreen5 == 0) {
                            BrainGame.Bind.ShowHide.Screen5();
                        }
                        if (LogicalCheckedQ1 + LogicalCheckedQ2 == 2) {
                            BrainGame.Bind.ShowHide.Screen5();
                        }
                        $("#TempScreen5").html(tempScreen5);
                    }, 900);
                }, 500);
            },
            Screen6: function () {
                clearTimeout(toScreen5);
                clearInterval(intScreen5);
                tempScreen6 = 5; toScreen6 = null; intScreen6 = null;
                toScreen6 = setTimeout(function () {
                    intScreen6 = setInterval(function () {
                        tempScreen6--;
                        if (tempScreen6 == 0) {
                            BrainGame.Bind.ShowHide.Screen6();
                        }
                    }, 900);
                }, 500);
            },
            Screen7: function () {
                clearTimeout(toScreen6);
                clearInterval(intScreen6);
                tempScreen7 = 30; toScreen7 = null; intScreen7 = null;
                //temp--;
                $("#TempScreen7").html(tempScreen7);
                toScreen7 = setTimeout(function () {
                    intScreen7 = setInterval(function () {
                        tempScreen7--;
                        if (tempScreen7 <= 5) {
                            var shutdownEngagedScreen7 = $("#shutdown_engaged")[0];
                            shutdownEngagedScreen7.play();
                            $("#ClickMeScreen7").addClass("timer-watch2");

                        }
                        if (tempScreen7 == 0) {
                            BrainGame.Bind.ShowHide.Screen7();

                        }
                        if (LogicalCheckedQ3 + LogicalCheckedQ4 == 2) {
                            BrainGame.Bind.ShowHide.Screen7();
                        }
                        $("#TempScreen7").html(tempScreen7);
                    }, 900);
                }, 500);
            },
            FirstLevel: function () {
                clearTimeout(toScreen7);
                clearInterval(intScreen7);
                tempL1 = 30; toL1 = null; intL1 = null;
                $("#TempLevel1").html(tempL1);
                toL1 = setTimeout(function () {
                    intL1 = setInterval(function () {
                        tempL1--;
                        if (tempL1 <= 5) {
                            shutdownEngagedL1 = $("#shutdown_engaged")[0];
                            shutdownEngagedL1.play();
                            $("#ClickMeLevel1").addClass("timer-watch2");
                        }
                        if (tempL1 == 0) {
                            BrainGame.Bind.ShowHide.FirstLevel();
                        }
                        //$check = parseInt($('#check').val());
                        //if ($check == 2) {
                        //    BrainGame.Bind.ShowHide.FirstLevel();
                        //}
                        $("#TempLevel1").html(tempL1);
                    }, 900);
                }, 500);
            },
            SecondLevel: function () {
                clearTimeout(toL1);
                clearInterval(intL1);
                tempL2 = 30; toL2 = null; intL2 = null;
                $("#TempLevel2").html(tempL2);
                toL2 = setTimeout(function () {
                    intL2 = setInterval(function () {
                        tempL2--;
                        if (tempL2 <= 5) {
                            shutdownEngagedL2 = $("#shutdown_engaged")[0];
                            shutdownEngagedL2.play();
                            $("#ClickMeLevel2").addClass("timer-watch2");

                        }
                        if (tempL2 == 0) {
                            BrainGame.Bind.ShowHide.SecondLevel();
                        }
                        //$check = parseInt($('#check').val());
                        //if ($check == 2) {
                        //    BrainGame.Bind.ShowHide.FirstLevel();
                        //}
                        $("#TempLevel2").html(tempL2);
                    }, 900);
                }, 500);
            },
            ThirdLevel: function () {
                clearTimeout(toL2);
                clearInterval(intL2);
                tempL3 = 30; toL3 = null; intL3 = null;
                $("#TempLevel3").html(tempL3);
                toL3 = setTimeout(function () {
                    intL3 = setInterval(function () {
                        tempL3--;
                        if (tempL3 <= 5) {
                            shutdownEngagedL3 = $("#shutdown_engaged")[0];
                            shutdownEngagedL3.play();
                            $("#ClickMeLevel3").addClass("timer-watch2");
                        }
                        if (tempL3 == 0) {
                            BrainGame.Bind.ShowHide.ThirdLevel();
                        }
                        //$check = parseInt($('#check').val());
                        //if ($check == 2) {
                        //    BrainGame.Bind.ShowHide.ThirdLevel();
                        //}
                        $("#TempLevel3").html(tempL3);
                    }, 900);
                }, 500);
            }
        },
        ShowHide: {
            Screen1: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "block");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
            },
            Screen2: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "block");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
            },
            Screen3: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "block");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
            },
            Screen4_1: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "block");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
            },
            Screen4_2: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "block");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
            },
            Screen4_3: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "block");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
            },
            Screen4_4: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "block");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
            },
            Screen4_5: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "block");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");

                //also add timer hear
                BrainGame.Bind.Timer.Screen4_6();
            },
            Screen4_6: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "block");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");

                BrainGame.Bind.Timer.Screen4_7();
                //start timer
            },
            Screen4_7: function () {
                clearTimeout(toScreen4_7);
                clearInterval(intScreen4_7);
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "block");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
            },
            Screen4_8: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");
                if (Age >= 5 && Age <= 6) {
                    BrainGame.Bind.ShowHide.Screen7();
                }
                else {
                    $("#divscreen5").css("display", "block");
                    $("#divscreen6").css("display", "none");
                    $("#divscreen7").css("display", "none");
                    $("#divLevel1").css("display", "none");
                    $("#divLevel2").css("display", "none");
                    $("#divLevel3").css("display", "none");
                    $("#divLevelresult").css("display", "none");
                    BrainGame.Bind.Ajax.FirstLogical(Age);
                    BrainGame.Bind.Timer.Screen5();
                }
            },
            Screen5: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                if (Age >= 5 && Age <= 6) {
                    BrainGame.Bind.ShowHide.Screen7();
                }
                else {
                    $("#divscreen6").css("display", "block");
                    $("#divscreen7").css("display", "none");
                    $("#divLevel1").css("display", "none");
                    $("#divLevel2").css("display", "none");
                    $("#divLevel3").css("display", "none");
                    $("#divLevelresult").css("display", "none");
                    BrainGame.Bind.Timer.Screen6();
                }
            },
            Screen6: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                if (Age >= 5 && Age <= 6) {
                    BrainGame.Bind.ShowHide.Screen7();
                }
                else {
                    $("#divscreen7").css("display", "block");
                    $("#divLevel1").css("display", "none");
                    $("#divLevel2").css("display", "none");
                    $("#divLevel3").css("display", "none");
                    $("#divLevelresult").css("display", "none");
                    BrainGame.Bind.Ajax.SecondLogical(Age);
                    BrainGame.Bind.Timer.Screen7();
                }

            },
            Screen7: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "block");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
                BrainGame.Bind.Timer.FirstLevel();
            },
            FirstLevel: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "block");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "none");
                BrainGame.Bind.Timer.SecondLevel();
            },
            SecondLevel: function () {
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "block");
                $("#divLevelresult").css("display", "none");
                BrainGame.Bind.Timer.ThirdLevel();
            },
            ThirdLevel: function () {
                clearTimeout(toL3);
                clearInterval(intL3);
                $("#divScreenSize1").css("display", "none");
                $("#divScreenSize2").css("display", "none");
                $("#divScreenSize3").css("display", "none");
                $("#divScreenSize4-1").css("display", "none");
                $("#divScreenSize4-2").css("display", "none");
                $("#divScreenSize4-3").css("display", "none");
                $("#divScreenSize4-4").css("display", "none");
                $("#divScreenSize4-5").css("display", "none");
                $("#divScreenSize4-6").css("display", "none");
                $("#divScreenSize4-7").css("display", "none");
                $("#divScreenSize4-8").css("display", "none");

                $("#divscreen5").css("display", "none");
                $("#divscreen6").css("display", "none");
                $("#divscreen7").css("display", "none");
                $("#divLevel1").css("display", "none");
                $("#divLevel2").css("display", "none");
                $("#divLevel3").css("display", "none");
                $("#divLevelresult").css("display", "block");

                $("#resultLevel1").text(correct_Level1);
                $("#resultLevel2").text(correct_Level2);
                $("#resultLevel3").text(correct_Level3);
                result = correct_Level1 + correct_Level2 + correct_Level3;
                $("#spanTotal").text(result);
                 $("#spanTotalCup").text(result);
                LogicalCorrectAns = ans1 + ans2 + ans3 + ans4;
                $("#spanLogical").text(LogicalCorrectAns + " Correct out of 4");
            }
        },
        FirstLevel: {
            box_fist: function (result) {
                $('#first').removeClass('active-bg');
                $('#first').addClass('dactive-bg');
                $('#first').addClass('box1');
                $('#second').removeClass('dactive-bg');
                $('#second').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_second: function (result) {
                $('#second').removeClass('active-bg');
                $('#second').addClass('dactive-bg');
                $('#second').addClass('box2');

                $('#third').removeClass('dactive-bg');
                $('#third').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_third: function (result) {
                $('#third').removeClass('active-bg');
                $('#third').addClass('dactive-bg');
                $('#third').addClass('box3');

                $('#four').removeClass('dactive-bg');
                $('#four').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;

            },
            box_four: function (result) {
                $('#four').removeClass('active-bg');
                $('#four').addClass('dactive-bg');
                $('#four').addClass('box4');

                $('#five').removeClass('dactive-bg');
                $('#five').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_five: function (result) {
                $('#five').removeClass('active-bg');
                $('#five').addClass('dactive-bg');
                $('#five').addClass('box5');

                $('#six').removeClass('dactive-bg');
                $('#six').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_six: function (result) {
                $('#six').removeClass('active-bg');
                $('#six').addClass('dactive-bg');
                $('#six').addClass('box6');

                $('#seven').removeClass('dactive-bg');
                $('#seven').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_seven: function (result) {
                $('#seven').removeClass('active-bg');
                $('#seven').addClass('dactive-bg');
                $('#seven').addClass('box7');

                $('#eight').removeClass('dactive-bg');
                $('#eight').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_eight: function (result) {
                $('#eight').removeClass('active-bg');
                $('#eight').addClass('dactive-bg');
                $('#eight').addClass('box8');

                $('#nine').removeClass('dactive-bg');
                $('#nine').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_nine: function (result) {
                $('#nine').removeClass('active-bg');
                $('#nine').addClass('dactive-bg');
                $('#nine').addClass('box9');

                $('#ten').removeClass('dactive-bg');
                $('#ten').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_ten: function (result) {
                $('#ten').removeClass('active-bg');
                $('#ten').addClass('dactive-bg');
                $('#ten').addClass('box10');

                $('#eleven').removeClass('dactive-bg');
                $('#eleven').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_eleven: function (result) {
                $('#eleven').removeClass('active-bg');
                $('#eleven').addClass('dactive-bg');
                $('#eleven').addClass('box11');

                $('#twelve').removeClass('dactive-bg');
                $('#twelve').addClass('active-bg');
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
            },
            box_twelve: function (result) {
                if (result == "1")
                    correct_Level1 = correct_Level1 + 1;
                BrainGame.Bind.ShowHide.FirstLevel();
            }
        },// Close FirstLevel
        SecondLevel: {
            box_fist: function (result) {
                $('#first_Level2').removeClass('active-bg');
                $('#first_Level2').addClass('dactive-bg');
                $('#first_Level2').addClass('box1');
                $('#second_Level2').removeClass('dactive-bg');
                $('#second_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_second: function (result) {
                $('#second_Level2').removeClass('active-bg');
                $('#second_Level2').addClass('dactive-bg');
                $('#second_Level2').addClass('box2');

                $('#third_Level2').removeClass('dactive-bg');
                $('#third_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_third: function (result) {
                $('#third_Level2').removeClass('active-bg');
                $('#third_Level2').addClass('dactive-bg');
                $('#third_Level2').addClass('box3');

                $('#four_Level2').removeClass('dactive-bg');
                $('#four_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;

            },
            box_four: function (result) {
                $('#four_Level2').removeClass('active-bg');
                $('#four_Level2').addClass('dactive-bg');
                $('#four_Level2').addClass('box4');

                $('#five_Level2').removeClass('dactive-bg');
                $('#five_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_five: function (result) {
                $('#five_Level2').removeClass('active-bg');
                $('#five_Level2').addClass('dactive-bg');
                $('#five_Level2').addClass('box5');

                $('#six_Level2').removeClass('dactive-bg');
                $('#six_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_six: function (result) {
                $('#six_Level2').removeClass('active-bg');
                $('#six_Level2').addClass('dactive-bg');
                $('#six_Level2').addClass('box6');

                $('#seven_Level2').removeClass('dactive-bg');
                $('#seven_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_seven: function (result) {
                $('#seven_Level2').removeClass('active-bg');
                $('#seven_Level2').addClass('dactive-bg');
                $('#seven_Level2').addClass('box7');

                $('#eight_Level2').removeClass('dactive-bg');
                $('#eight_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_eight: function (result) {
                $('#eight_Level2').removeClass('active-bg');
                $('#eight_Level2').addClass('dactive-bg');
                $('#eight_Level2').addClass('box8');

                $('#nine_Level2').removeClass('dactive-bg');
                $('#nine_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_nine: function (result) {
                $('#nine_Level2').removeClass('active-bg');
                $('#nine_Level2').addClass('dactive-bg');
                $('#nine_Level2').addClass('box9');

                $('#ten_Level2').removeClass('dactive-bg');
                $('#ten_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_ten: function (result) {
                $('#ten_Level2').removeClass('active-bg');
                $('#ten_Level2').addClass('dactive-bg');
                $('#ten_Level2').addClass('box10');

                $('#eleven_Level2').removeClass('dactive-bg');
                $('#eleven_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_eleven: function (result) {
                $('#eleven_Level2').removeClass('active-bg');
                $('#eleven_Level2').addClass('dactive-bg');
                $('#eleven_Level2').addClass('box11');

                $('#twelve_Level2').removeClass('dactive-bg');
                $('#twelve_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_twelve: function (result) {
                $('#twelve_Level2').removeClass('active-bg');
                $('#twelve_Level2').addClass('dactive-bg');
                $('#twelve_Level2').addClass('box12');

                $('#thirteen_Level2').removeClass('dactive-bg');
                $('#thirteen_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_thirteen: function (result) {
                $('#thirteen_Level2').removeClass('active-bg');
                $('#thirteen_Level2').addClass('dactive-bg');
                $('#thirteen_Level2').addClass('box13');

                $('#fourteen_Level2').removeClass('dactive-bg');
                $('#fourteen_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_fourteen: function (result) {
                $('#fourteen_Level2').removeClass('active-bg');
                $('#fourteen_Level2').addClass('dactive-bg');
                $('#fourteen_Level2').addClass('box14');

                $('#fifthteen_Level2').removeClass('dactive-bg');
                $('#fifthteen_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_fifthteen: function (result) {
                $('#fifthteen_Level2').removeClass('active-bg');
                $('#fifthteen_Level2').addClass('dactive-bg');
                $('#fifthteen_Level2').addClass('box15');

                $('#sixteen_Level2').removeClass('dactive-bg');
                $('#sixteen_Level2').addClass('active-bg');
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
            },
            box_sixteen: function (result) {
                if (result == "1")
                    correct_Level2 = correct_Level2 + 1;
                BrainGame.Bind.ShowHide.SecondLevel();
            }
        },// Close SecondLevel
        ThirdLevel: {
            box_fist: function (result) {
                $('#first_Level3').removeClass('active-bg');
                $('#first_Level3').addClass('dactive-bg');
                $('#first_Level3').addClass('fade1');
                $('#second_Level3').removeClass('dactive-bg');
                $('#second_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_second: function (result) {
                $('#second_Level3').removeClass('active-bg');
                $('#second_Level3').addClass('dactive-bg');
                $('#second_Level3').addClass('fade2');

                $('#third_Level3').removeClass('dactive-bg');
                $('#third_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_third: function (result) {
                $('#third_Level3').removeClass('active-bg');
                $('#third_Level3').addClass('dactive-bg');
                $('#third_Level3').addClass('fade3');

                $('#four_Level3').removeClass('dactive-bg');
                $('#four_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;

            },
            box_four: function (result) {
                $('#four_Level3').removeClass('active-bg');
                $('#four_Level3').addClass('dactive-bg');
                $('#four_Level3').addClass('fade4');

                $('#five_Level3').removeClass('dactive-bg');
                $('#five_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_five: function (result) {
                $('#five_Level3').removeClass('active-bg');
                $('#five_Level3').addClass('dactive-bg');
                $('#five_Level3').addClass('fade5');

                $('#six_Level3').removeClass('dactive-bg');
                $('#six_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_six: function (result) {
                $('#six_Level3').removeClass('active-bg');
                $('#six_Level3').addClass('dactive-bg');
                $('#six_Level3').addClass('fade6');

                $('#seven_Level3').removeClass('dactive-bg');
                $('#seven_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_seven: function (result) {
                $('#seven_Level3').removeClass('active-bg');
                $('#seven_Level3').addClass('dactive-bg');
                $('#seven_Level3').addClass('fade7');

                $('#eight_Level3').removeClass('dactive-bg');
                $('#eight_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_eight: function (result) {
                $('#eight_Level3').removeClass('active-bg');
                $('#eight_Level3').addClass('dactive-bg');
                $('#eight_Level3').addClass('fade8');

                $('#nine_Level3').removeClass('dactive-bg');
                $('#nine_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_nine: function (result) {
                $('#nine_Level3').removeClass('active-bg');
                $('#nine_Level3').addClass('dactive-bg');
                $('#nine_Level3').addClass('fade9');

                $('#ten_Level3').removeClass('dactive-bg');
                $('#ten_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_ten: function (result) {
                $('#ten_Level3').removeClass('active-bg');
                $('#ten_Level3').addClass('dactive-bg');
                $('#ten_Level3').addClass('fade10');

                $('#eleven_Level3').removeClass('dactive-bg');
                $('#eleven_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_eleven: function (result) {
                $('#eleven_Level3').removeClass('active-bg');
                $('#eleven_Level3').addClass('dactive-bg');
                $('#eleven_Level3').addClass('fade11');

                $('#twelve_Level3').removeClass('dactive-bg');
                $('#twelve_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_twelve: function (result) {
                $('#twelve_Level3').removeClass('active-bg');
                $('#twelve_Level3').addClass('dactive-bg');
                $('#twelve_Level3').addClass('fade12');

                $('#thirteen_Level3').removeClass('dactive-bg');
                $('#thirteen_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_thirteen: function (result) {
                $('#thirteen_Level3').removeClass('active-bg');
                $('#thirteen_Level3').addClass('dactive-bg');
                $('#thirteen_Level3').addClass('fade13');

                $('#fourteen_Level3').removeClass('dactive-bg');
                $('#fourteen_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_fourteen: function (result) {
                $('#fourteen_Level3').removeClass('active-bg');
                $('#fourteen_Level3').addClass('dactive-bg');
                $('#fourteen_Level3').addClass('fade14');

                $('#fifthteen_Level3').removeClass('dactive-bg');
                $('#fifthteen_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_fifthteen: function (result) {
                $('#fifthteen_Level3').removeClass('active-bg');
                $('#fifthteen_Level3').addClass('dactive-bg');
                $('#fifthteen_Level3').addClass('fade15');

                $('#sixteen_Level3').removeClass('dactive-bg');
                $('#sixteen_Level3').addClass('active-bg');
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
            },
            box_sixteen: function (result) {
                if (result == "1")
                    correct_Level3 = correct_Level3 + 1;
                BrainGame.Bind.ShowHide.ThirdLevel();
            }
        }// Close ThirdLevel
    }//Close Bind
}//Close BrainGame 